<?php
	$class = '';
	$url = get_permalink();
	$img = get_field('lc-store__logo');
	$img = $img['sizes']['medium'];
	$name = get_the_title();
?>
<li class="store">
	<a href="<?php echo $url; ?>" class="store__link"></a>
	<h2 class="store__name"><?php echo $name; ?></h2>
	<img src="<?php echo $img; ?>" class="store__logo"/>
</li>