<?php
	$GLOBALS['hide_contact_form'] = false;
	$GLOBALS['expand_contact_form'] = false;
?>
<?php include 'includes/nav-icons.php'; // set the icons (FontAwesome) ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	
	<?php if (!empty(get_theme_mod('tracking_google'))) : ?>
		<!-- GA Tracking -->
		<?php echo get_theme_mod('tracking_google'); ?>
		<!-- / GA Tracking -->
	<?php endif; ?>
	
	<?php if (!empty(get_theme_mod('tracking_facebook'))) : ?>
		<!-- FB Pixel Tracking -->
		<?php echo get_theme_mod('tracking_facebook'); ?>
		<!-- / FB Pixel Tracking -->
	<?php endif; ?>
</head>
<body <?php body_class('no-js no-jq'); ?>>

<?php /* HEADER/NAVIGATION */ ?>
<section id="header">
	
	<nav id="utility-bar" class="utility hidden-sm">
		<?php
			wp_nav_menu(array(
					'theme_location' => 'top-menu',
					'depth' => 1,
					'container' => 'nav',
					'container_class' => 'utility__menu-container',
					'container_id' => 'utility-menu-container',
					'menu_class' => 'utility__menu',
					'menu_id' => 'utility-menu',
			));
		?>
		
		<div class="utility__social social-menu">
			<?php
				if ($social_icons_html = lc_get_icon_links($icons['social'])) { // see includes/nav-icons.php
					echo $social_icons_html;
				}
			?>
		</div>
	</nav>
	
	<nav role="navigation" class="top-nav" id="main-navigation">
		<div class="navbar navbar-expand-lg navbar-light">
			<a class="navbar-brand" href="<?php echo site_url(); ?>">
				<?php
					if ( !empty(get_theme_mod('custom_logo')) ) {
						$logo = wp_get_attachment_image(get_theme_mod('custom_logo'), 'large',true, array('class'=>'logo__image') );
					} else {
						$logo = '<img class="logo__image" src="' . get_template_directory_uri() . '/images/logo.svg' . '" />';
					}
					
					echo '<div class="logo">' . $logo . '</div>';
					?>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wrapper"
							aria-controls="navbar-wrapper" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			
			<div id="navbar-wrapper" class="navbar__wrapper collapse navbar-collapse">
				<?php
					wp_nav_menu(array(
							'theme_location' => 'primary-menu',
							'depth' => 2,
							'container' => 'nav',
							'container_class' => 'menu-main-nav-container',
							'container_id' => 'global-nav',
							'menu_class' => 'navbar-nav',
							'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
							'walker' => new WP_Bootstrap_Navwalker()
					));
				?>
			</div>
			
		</div>
	</nav>
</section>
<?php /* / HEADER/NAVIGATION */ ?>

<?php /* HERO/SLIDER AREA */ ?>
<?php
	$hero_class = $hero_style = $hero_media = false;
	
	$post_id = $post->ID;
	if ( is_home() ) {
		$post_id = get_option( 'page_for_posts' );
	}
	
	$modes = [ 'none', 'single','random','slideshow','video' ];
	$mode  = $modes[0];
	if ( !empty(get_field('lc-hero__type', $post_id)) ) {
		$selected_mode = strtolower(get_field('lc-hero__type', $post_id));
		
		if ( in_array( $selected_mode, $modes ) ) {
			$mode = $selected_mode;
		}
	}
	
	$hero_class = 'hero--' . $mode;
	
	switch($mode) :
		case 'single' :
			if (!empty(get_field('lc-hero__image', $post_id))) :
				$img = get_field('lc-hero__image', $post_id);
				$hero_style = 'background-image:url(\'' . $img['sizes']['hero'] . '\');';
			endif;
			break;
		case 'none' :
			if ( !empty(get_the_post_thumbnail()) ) :
				$img = get_the_post_thumbnail_url($post, 'hero');
				$hero_style = 'background-image:url(\'' . $img . '\');';
				$hero_class = 'hero--featured-image';
			else:
				$hero_class = 'hero--none';
			endif;
			break;
		case 'video' :
			$vurl = '';
			$video = get_field('lc-hero__video');
			
			if (!empty($video['video__id'])):
				if ( is_numeric($video['video__id']) ) :
					$vurl = 'https://player.vimeo.com/video/'.$video['video__id'];
				elseif ( filter_var($video['video__id'], FILTER_VALIDATE_URL) !== FALSE ) :
					$vurl = $video['video__id'];
				endif;
				
				if ( !empty($vurl) ) :
					$hero_class = "hero--video";
					$hero_media = '<iframe class="hero__video-overlay" src="' . $vurl . '?background=1&autoplay=1&loop=1&byline=0&title=0&muted=1" frameborder="0" allow="autoplay" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
				endif;
				
				if ( !empty($video['video__preview_image']) ) :
					$hero_style = 'background-image:url(\''.$video['video__preview_image']['sizes']['hero'].'\')';
				endif;
			endif;
			break;
		case 'random' :
			if ( !empty(get_field('lc-hero__images')) ) :
				$images = get_field('lc-hero__images');
				$image = $images[rand(0,count($images)-1)];
				$hero_style = 'background-image:url(\''.$image['sizes']['hero'].'\')';
			endif;
			break;
		case 'slideshow' :
			if ( !empty(get_field('lc-hero__images')) ) :
				$images = get_field('lc-hero__images');
				shuffle($images);
				$hero_media = '';
				foreach ( $images as $image ) :
					$hero_media .= '<div class="hero__image" style="background-image:url(\''.$image['sizes']['hero'].'\'"></div>';
				endforeach;
				$hero_media = '<div class="hero__images">'.$hero_media.'</div>';
			endif;
			break;
		default :
			if ( !empty(get_the_post_thumbnail()) ) :
				$img = get_the_post_thumbnail_url($post, 'hero');
				$hero_style = 'background-image:url(\'' . $img . '\');';
			endif;
			break;
	endswitch;
	
	if ( empty($hero_style) ) :
		if ( !empty(get_the_post_thumbnail()) ) :
			$img = get_the_post_thumbnail_url($post, 'hero');
			$hero_style = 'background-image:url(\'' . $img . '\');';
		else:
			$fb_header = get_theme_mod('fallback_header_image');
			$img = wp_get_attachment_image_src(get_theme_mod('fallback_header_image'), 'hero');
			$img = $img[0]; // url
			$hero_style = 'background-image:url(\'' . $img . '\');';
		endif;
	endif;
	
	if ( empty($hero_style) && empty($hero_media) ) :
		$hero_class = 'hero--default';
	endif;
		
	// VIDEO
	if ( !empty(get_field('lc-hero__video-overlay'))) {
		$vurl = '';
		$vo = get_field('lc-hero__video-overlay');
		
		if ( is_numeric($vo) ) {
			$vurl = 'https://player.vimeo.com/video/'.$vo;
		} elseif ( filter_var($vo, FILTER_VALIDATE_URL) !== FALSE ) {
			$vurl = $vo;
		}
		
		if ( !empty($vurl) ) {
			$hero_class = "hero--video";
			$video_overlay = '<iframe class="hero__video-overlay" src="' . $vurl . '?background=1&autoplay=1&loop=1&byline=0&title=0&muted=1" frameborder="0" allow="autoplay" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		}
	}
	
	if ( empty($hero_style) && !empty(get_the_post_thumbnail()) ) {
		$img = get_the_post_thumbnail_url($post, 'hero');
		$hero_style .= 'background-image:url(\'' . $img . '\');';
	}
?>
<section id="hero"
	class="hero<?php echo !empty($hero_class) ? ' ' . $hero_class : '' ?>"
	<?php echo !empty($hero_style) ? 'style="'.$hero_style.'"' : '' ?>>
	<?php echo !empty($hero_media) ? $hero_media : ''; ?>
	<?php if ( is_front_page() ) : ?>
	<div class="container-fluid">
		<?php echo !empty($logo) ? '<div class="hero__logo">'.$logo.'</div>' : '' ?>
	</div>
	<?php endif; ?>
</section>

<a name="start" id="start"></a>