<?php
	/**
	 * Template Name: Page without contact form
	 */
	get_header();
	$GLOBALS['hide_contact_form'] = true;
?>

<?php
	/* CONTENT */
	if (have_posts()): $title = get_the_title(); // default, but let's pull the category (and go back?? maybe use session for this)
		$title = mark_first_word($title, 'first', 'alt');
		?>
		
		<header class="content__header" id="content-header">
			<div class="container-fluid">
				<h1 class="content__title"><?php echo $title; ?></h1>
			</div>
		</header>
		
		<section id="content">
			<div class="container-fluid">
				<?php
					while (have_posts()):
						the_post();
						
						the_content();
					endwhile;
				?>
			</div>
		</section>
	<?php
	endif;
	/* END CONTENT */
	
	
	
	get_footer();