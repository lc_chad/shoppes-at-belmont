<?php
session_start();

require_once 'includes/vendor/autoload.php';
use Spatie\OpeningHours\OpeningHours;

get_header('store');

/*ACF
    =====
    lc-store__logo
    lc-store__description
    lc-store__photo
    lc-store__hours
    lc-store__phone
    lc-store__address
    lc-store__building
    lc-store__suite
    lc-store__address
    lc-store__url
    lc-store__social
    lc-store--anchor
    */

$store_class =
$store_html =
$store__photo =
$store__hours =
$store__phone =
$store__address =
$store__url =
$store__directions =
$store__social =
$store__logo = false;

$store__title = get_the_title();


if (!empty(get_field('lc-store__logo'))) {
    $store__logo_class = '';

    $store__logo = get_field('lc-store__logo');
    $store__logo = $store__logo['sizes']['large'];
    $store_class .= ' store--with-logo';

    list($width, $height, $type, $attr) = getimagesize($store__logo);

    if ( $height > $width ) {
        $store__logo_class .= ' store--tall-logo';
    }
}

if (!empty(get_field('lc-store__photo'))) {
    $store__photo = get_field('lc-store__photo');
    $store__photo = $store__photo['sizes']['large'];
    $store_class .= ' store--with-photo';
}

if (!empty(get_field('lc-store__hours'))) {
    $days = [];
    $raw_days = get_field('lc-store__hours');

    foreach ( $raw_days as $k=>$v ) {
        $day_name = explode('__', $k); // splitting up hours__dayname
        $day_name = $day_name[1];

        if (trim(strtolower($v)) === 'closed') {
            // empty
            $days[$day_name] = [];
        } else {
            $times = $v;
            // format time too
            $times = explode(' ', trim($times));

            foreach ($times as $time) {
                if (strpos($time, '-') > 0) { // TODO: split up multiple time slots??
                    $time = explode('-', $v);
                    $start = date('H:i', strtotime($time[0]));
                    $end = date('H:i', strtotime($time[1]));
                    $time = $start . '-' . $end;
                    $days[$day_name][] = $time;
                }
            }
        }
    }

    if ( count($days) > 0 ) {
        $business_hours = OpeningHours::create($days, 'America/New_York');
        $hours = $business_hours->forWeekCombined();

        foreach ($hours as $starting_day => $details) {
            $count = count($details['days']);
            $days_string = ucfirst($starting_day);

            if ($count > 1) {
                $days_string = ucfirst($starting_day) . ' &ndash; ' . ucfirst($details['days'][$count - 1]);
            }

            $opening_hours = $details['opening_hours']->getIterator();

            $hours_string = '';
            while ($opening_hours->valid()) {
                $start = date('g:iA', strtotime($opening_hours->current()->start()));
                $end = date('g:iA', strtotime($opening_hours->current()->end()));

                $hours_string .= $start . ' &ndash; ' . $end;
                $opening_hours->next();
            }

            if (empty($hours_string)) {
                $hours_string = 'Closed';
            }

            $store__hours .= '<div class="store__day"><div class="day__name">' . $days_string . '</div><div class="day__hours">' . $hours_string . '</div></div>';
        }

        $open_status = '<div class="store__open-status store__open-status--closed">Closed right now</div>';
        $hours_class = 'store__hours--closed';
        if ($business_hours->isOpen()) {
            $hours_class = 'store__hours--open';
            $open_status = '<div class="store__open-status store__open-status--open">Open right now</div>';
        }

        $store__hours = '<div class="store__hours">' . $open_status . $store__hours . '</div>';
    }
}

if (!empty(get_field('lc-store__phone'))) {
    $store__phone = get_field('lc-store__phone');

    $store__phone = '<a href="tel:'.$store__phone.'">'.$store__phone.'</a>';
}

if (!empty(get_field('lc-store__url'))) {
    $store__url = '<a class="store__button btn btn--primary button" href="'.get_field('lc-store__url').'" target="_blank">Visit Website +</a>';
}

if (!empty(get_field('lc-store__address'))) {
    $address = get_field('lc-store__address');
    $lat = $address['lat'];
    $lon = $address['lng'];
    $address = $address['address'];
    $building = get_field('lc-store__building');
    $suite = get_field('lc-store__suite');

    $store__address = $address;
    if ( !empty($building) ) {
        $store__address .= '<br />Building ' . $building;
    }
    if ( !empty($suite) ) {
        $store__address .= ', Suite ' . $suite;
    }

    $store__directions = '<a target="_blank" class="store__button btn btn--primary button" href="https://www.google.com/maps/place/' . urlencode($address) . '">Get directions +</a>';
}

if (!empty(get_field('lc-store__social'))) {
    $socials = get_field('lc-store__social');

    foreach ( $socials as $social ) {
        $url = $social['social__url'];
        $class = $social['social__network'];
        $store__social .= '<div class="store__social"><a href="'.$url.'" target="_blank" class="social__link "><i class="fab fa-' . $class . '"></i></a></div>';
    }
}

$dir_link = '/shop';
$title = 'Dining &amp; Attractions';
if ( !empty($_SESSION['directory_url']) ):
    $dir_link = $_SESSION['directory_url'];
    $title = get_the_title(url_to_postid($dir_link));
endif;

if ( !empty(get_field('lc-store__type')) ):
    $type = get_field('lc-store__type');
endif;

$title = mark_first_word($title, 'first', 'alt');
?>

    <header id="content-header" class="content__header">
        <div class="container-fluid">
            <h1 class="content__title"><?php echo $title; ?></h1>
        </div>

        <nav class="content__nav">
            <div class="container-fluid content__nav-container">
                <a href="<?php echo $dir_link; ?>" class="nav__return">&laquo; Return to Directory</a>
                <a href="/map" class="nav__map-link btn button">Directory Map</a>
            </div>
        </nav>
    </header>

    <section id="content" class="content">
        <div class="container-fluid">
            <div class="store <?php echo $store_class; ?>">
                <?php if (!empty($store__photo)) : ?>
                    <div class="store__photo" style="background-image:url('<?php echo $store__photo; ?>');">
                        <img src="<?php echo $store__photo; ?>" class="photo__image"/>
                    </div>

                <?php endif; ?>

                <div class="store__intro">
                    <?php if (!empty($store__logo)) : ?>
                        <div class="store__logo <?= $store__logo_class; ?>"><img src="<?php echo $store__logo; ?>" title="<?php echo $store__title; ?>"
                                                                                 alt="<?php echo $store__title; ?>" class="store__logo-image" /></div>
                    <?php endif; ?>
                    <h2 class="store__title"><?php echo $store__title; ?></h2>
                    <div class="store__description"><?php echo get_field('lc-store__description'); ?></div>
                </div>

                <div class="store__details">

                    <?php if (!empty($store__hours)) : ?>
                        <div class="store__detail store__detail--hours">
                            <h6 class="detail__title">Hours</h6>
                            <?php echo $store__hours; ?>
                        </div>
                    <?php endif; ?>

                    <div class="store__detail store__detail--contact">
                        <?php if (!empty($store__phone)) : ?>
                            <h6 class="detail__title">Phone</h6>
                            <?php echo $store__phone; ?>
                        <?php endif; ?>

                        <?php if (!empty($store__address)) : ?>
                            <h6 class="detail__title">Address</h6>
                            <?php echo $store__address; ?>
                        <?php endif; ?>
                    </div>

                    <div class="store__detail store__detail--connect">
                        <div class="store__ctas">
                            <?php if (!empty($store__url)) : ?>
                                <?php echo $store__url; ?>
                            <?php endif; ?>

                            <?php if (!empty($store__directions)) : ?>
                                <?php echo $store__directions; ?>
                            <?php endif; ?>
                        </div>

                        <div class="store__socials">
                            <?php if (!empty($store__social)) : ?>
                                <?php echo $store__social; ?>
                            <?php endif; ?>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </section>

<?php get_footer(); ?>