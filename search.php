<?php get_header(); ?>
  <section id="intro">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h1 class="page--title"><?php echo sprintf(
		          __( 'Search Results for &#8220;%s&#8221;' ),
		          get_search_query()
	          ); ?></h1>
        </div>
      </div>
    </div>
  </section>
  <section id="content">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
			<?php
			if ( have_posts() ):
				while ( have_posts() ):
					the_post();
					?>
                  <h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
					<?php
					the_excerpt();
					?>
                  <a href="<?php echo get_permalink(); ?>">View page</a>
                  <hr/>
				<?php
				endwhile;
				echo '<div class="text-center well lead">';
				echo paginate_links( array(
					'prev_text' => '<<',
					'next_text' => '>>',
				) );
				echo '</div>';
			else:
				?>
      <p>No results.</p>
			<?php
			endif;
			?>
        </div>
      </div>
    </div>
  </section>
<?php get_footer(); ?>