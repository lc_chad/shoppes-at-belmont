<?php include 'includes/nav-icons.php'; // set the icons (FontAwesome) ?>

<?php
	if (!$GLOBALS['hide_contact_form']) {
		include 'includes/contact.php';
	}
	
	$lat = '';
	$lng = '';
	
	if ( !empty(get_theme_mod('lc_map_lat')) ) :
		$lat = get_theme_mod('lc_map_lat');
	endif;
	
	if ( !empty(get_theme_mod('lc_map_lng')) ) :
		$lng = get_theme_mod('lc_map_lng');
	endif;
?>
	
	<section id="map" class="map">
		<div class="map__container">
			<!--Map Canvas-->
			<div class="map__canvas map-canvas"
					 data-zoom="13"
					 data-lat="<?php echo $lat; ?>"
					 data-lng="<?php echo $lng; ?>"
					 data-type="roadmap"
					 data-disableui="true"
					 data-draggable="false"
					 data-zoomcontrol="false"
					 data-hue="#EFEFEF">
			</div>
		</div>
	</section>
	
	<footer id="footer" class="footer">
		<div class="footer__wrapper">
			<?php if (is_active_sidebar('footer__badge')) : ?>
				<?php dynamic_sidebar('footer__badge'); ?>
			<?php endif; ?>
			
			<div class="footer__details">
				<?php if (!empty(get_theme_mod('custom_footer_logo'))): ?>
					<div class="footer__logo">
						<img class="logo logo--raster" src="<?php echo get_theme_mod('custom_footer_logo'); ?>"
								 alt="<?php echo get_bloginfo('site_title'); ?>"/>
					</div>
				<?php endif; ?>
				
				<div class="footer__contact">
					<?php if (!empty(get_theme_mod('site_address'))): ?>
						<div class="contact__address"><?php echo get_theme_mod('site_address'); ?></div>
					<?php endif; ?>
					<?php if (!empty(get_theme_mod('site_phone'))): ?>
						<div class="contact__phone">Phone: <a
											href="tel:<?php echo get_theme_mod('site_phone'); ?>"><?php echo get_theme_mod('site_phone'); ?></a>
						</div>
					<?php endif; ?>
				</div>
				
				<div class="footer__menu">
					<?php
						wp_nav_menu(array(
								'theme_location' => 'footer-menu',
								'depth' => 1,
								'container' => 'nav',
								'container_class' => 'footer__nav',
								'container_id' => 'footer-nav'
						));
					?>
				</div>
				
				<div class="footer__site-details">
					&copy; Copyright <?php echo date("Y"); ?> <?php echo get_bloginfo('site_title'); ?>. All Rights Reserved.
					| Website Powered and Managed by
					<a href="https://www.liquifiedcreative.com" target="_blank">Liquified Creative</a>.
				</div>
			</div>
			
			<div class="footer__social social-menu">
				<?php
					if ($social_icons_html = lc_get_icon_links($icons['social'])) { // see includes/nav-icons.php
						echo $social_icons_html;
					}
				?>
			</div>
		</div>
	</footer>


<?php if (!empty(get_theme_mod('tracking_callrail'))) : ?>
	<!-- DNI/Other Tracking -->
	<?php echo get_theme_mod('tracking_callrail'); ?>
	<!-- / DNI/Other Tracking -->
<?php endif; ?>

<?php wp_footer();