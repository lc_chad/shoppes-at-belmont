<?php get_header(); ?>
	<?php /*
	<section class="content-header">
		<div class="container-fluid">
			<?php the_title('<h1 class="content-header__title">', '</h1>'); ?>
		</div>
	</section>
*/ ?>
<?php
	/* CONTENT */
	if (have_posts()): ?>
		<section id="content">
			<div class="container-fluid">
				<?php
					while (have_posts()):
						the_post();
						?>
						<div class="post">
							<h4 class="post__date"><?php echo get_the_date( 'F jS, Y'); ?></h4>
							<div class="post__body">
								<?php the_content(); ?>
							</div>
						</div>
					<?php
					endwhile;
				?>
			</div>
		</section>
	<?php
	endif;
	/* END CONTENT */
	
	get_footer();