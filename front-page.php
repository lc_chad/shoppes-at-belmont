<?php get_header(); ?>

<?php include 'includes/intro.php'; ?>

<?php
	echo do_shortcode('[lc_ctas limit="4"]');
?>

<?php
	/* CONTENT */
	if (have_posts()): ?>
		<section id="content">
			<div class="container-fluid">
				<?php
					while (have_posts()):
						the_post();
						
						the_content();
					endwhile;
					
					wp_reset_postdata();
				?>
			</div>
		</section>
	<?php
	endif;
	/* END CONTENT */
?>

<?php /* FEATURED/ANCHOR STORES */ ?>
	<section id="featured-stores" class="featured-stores">
		<div class="container-fluid">
			<h4 class="featured-stores__title">Explore our Stores</h4>
			<?php
				// for now just set anchors on front page...
				if (!empty('lc-featured-stores')) : ?>
					<ul class="featured-stores__stores stores stores--featured">
						<div class="stores__header">Shop + Dine</div>
						<?php while (have_rows('lc-featured-stores')): the_row();
							$post = get_sub_field('store');
							setup_postdata($post);
							
							get_template_part( '/template-parts/post/store', 'grid' );
							
							/*
							$class = '';
							$url = get_permalink();
							$img = get_field('lc-store__logo');
							$img = $img['sizes']['medium'];
							$name = get_the_title();
							?>
							<li class="store">
								<a href="<?php echo $url; ?>" class="store__link"></a>
								<h2 class="store__name"><?php echo $name; ?></h2>
								<img src="<?php echo $img; ?>" class="store__logo"/>
							</li>
							<?php*/
							wp_reset_postdata();
						endwhile; ?>
						<li class="store store--view-more">
							<a href="/shop" class="store__link">View More</a>
						</li>
					</ul>
				<?php endif; ?>
		</div>
	</section>
<?php /* / FEATURED/ANCHOR STORES */ ?>

<?php /* RESIDENCES */ ?>
<?php if (!empty(get_field('lc-feature__title')) || !empty(get_field('lc-feature__description'))): ?>
	<section id="feature" class="feature">
		<div class="container-fluid">
			
			<div class="feature__info">
				<?php if (!empty(get_field('lc-feature__title'))) : ?>
				<h4 class="feature__title"><?php the_field('lc-feature__title'); ?></h4>
				<?php endif; ?>
				
				<?php if (!empty(get_field('lc-feature__description'))) : ?>
				<div class="feature__description"><?php the_field('lc-feature__description'); ?></div>
				<?php endif; ?>
			</div>
			
			<?php if (!empty(get_field('lc-feature__logo'))) :
				$img = get_field('lc-feature__logo');
				$img = $img['sizes']['hero'];?>
			<div class="feature__logo">
				<img src="<?php echo $img; ?>" />
			</div>
			<?php endif; ?>
		
		</div>
		
		<?php if (!empty(get_field('lc-feature__image'))) :
			$img = get_field('lc-feature__image');
			$img = $img['sizes']['hero'];?>
			<div class="feature__image" style="background-image:url('<?php echo $img; ?>');"></div>
		<?php endif; ?>
	</section>
<?php endif; ?>
<?php /* / RESIDENCES */ ?>

<?php require_once 'includes/news-items.php'; ?>

<?php /* EXPLORE */ ?>
<?php if ( !empty(get_field('lc-home-footer__title')) || !empty(get_field('lc-home-footer__image')) ) : ?>
	<section id="home-footer" class="home-footer">
		<div class="home-footer__header">
		<div class="container-fluid">
			<?php if (!empty(get_field('lc-home-footer__title'))) : ?>
				<h3 class="home-footer__title"><?php the_field('lc-home-footer__title'); ?></h3>
			<?php endif; ?>
			
			<?php if (!empty(get_field('lc-home-footer__subtitle'))) : ?>
				<h5 class="home-footer__subtitle"><?php the_field('lc-home-footer__subtitle'); ?></h5>
			<?php endif; ?>
		</div>
		</div>
		
		<?php if (!empty(get_field('lc-home-footer__image'))) :
			$img = get_field('lc-home-footer__image');
			$img = $img['sizes']['hero'];
			?>
			<div class="home-footer__image" style="background-image:url('<?php echo $img; ?>');"></div>
		<?php endif; ?>
	</section>
<?php endif; ?>
<?php /* / EXPLORE */ ?>

<?php
	
	get_footer();