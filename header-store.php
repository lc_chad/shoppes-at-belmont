<?php
	$GLOBALS['hide_contact_form'] = false;
	$GLOBALS['expand_contact_form'] = false;
?>
<?php
	function shoppes_render_title( $title ) {
		
		return $title;
	}
	add_filter( 'wp_title', 'shoppes_render_title', 10, 2 );
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body <?php body_class('no-js no-jq'); ?>>

<section id="header">
	
	<nav role="navigation" class="top-nav" id="main-navigation">
		<div class="navbar navbar-expand-lg navbar-light">
			<a class="navbar-brand" href="<?php echo site_url(); ?>">
				<?php
					if ( !empty(get_theme_mod('custom_logo')) ) {
						$logo = wp_get_attachment_image(get_theme_mod('custom_logo'), 'large',true, array('class'=>'logo__image') );
					} else {
						$logo = '<img class="logo__image" src="' . get_template_directory_uri() . '/images/logo.svg' . '" />';
					}
					
					$logo = '<div class="logo">'.$logo.'</div>';
					
					echo $logo;
					?>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wrapper"
							aria-controls="navbar-wrapper" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			
			<div id="navbar-wrapper" class="navbar__wrapper collapse navbar-collapse">
				<?php
					wp_nav_menu(array(
							'theme_location' => 'primary-menu',
							'depth' => 2,
							'container' => 'nav',
							'container_class' => 'menu-main-nav-container',
							'container_id' => 'global-nav',
							'menu_class' => 'navbar-nav',
							'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
							'walker' => new WP_Bootstrap_Navwalker()
					));
				?>
				
				<nav id="navbar-tools" class="navbar-tools">
					
					<?php include 'includes/nav-icons.php'; // set the icons (FontAwesome) ?>
					
					<?php
						if ($social_icons_html = lc_get_icon_links($icons['social'])) { // see includes/nav-icons.php
							echo $social_icons_html;
						}
					?>
				</nav>
			</div>
			
		</div>
	</nav>
</section>

<?php
	$hero_type = 'single';
	$img = '';
	
	if (has_post_thumbnail()) {
		$img = get_the_post_thumbnail_url();
	}
	
	?>
<section id="hero" class="hero" <?php echo !empty($img) ? 'style="background-image:url(\''.$img.'\')"' : ''; ?>>
	<div class="container-fluid">
		<?php
			/* TODO: light logo, slider/video if/else */
		?>
	</div>
</section>

<a name="start" id="start"></a>