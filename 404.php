<?php get_header();
	
	/* CONTENT */ ?>
		<section id="content">
			<div class="container-fluid">
				<h2>404</h2>
				<p>It appears the page you were looking for cannot be found.  Please navigate to a new page using the top navigation menu.</p>
			</div>
		</section>
	<?php
	/* END CONTENT */
	
	get_footer();