<?php get_header(); ?>
    <section id="intro" class="parallax-window" data-bleed="60" data-parallax="scroll"
             data-image-src="<?php echo(get_field('lc_intro_background') ? get_field('lc_intro_background')['url'] : get_template_directory_uri() . '/images/bg-intro-default.jpg'); ?>">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page--title"><?php single_cat_title(); ?></h1>
                </div>
            </div>
        </div>
       
    </section>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="posts--wrapper">
                        <?php
                        if (have_posts()):
                            ?>
                            <div class="row">
                                <?php
                                while (have_posts()):
                                    the_post();
                                    ?>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="news-card--wrapper">
                                            <?php

                                            $newsimage = get_template_directory_uri() . 'default-news.jpg';
                                            if (has_post_thumbnail()):
                                                if (wp_is_mobile()):
                                                    $newsimage = get_the_post_thumbnail_url(get_the_ID(), 'large');
                                                else:
                                                    $newsimage = get_the_post_thumbnail_url(get_the_ID(), 'medium');
                                                endif;
                                            endif;
                                            ?>
                                            <div class="news-card--image" style="background-image:url(<?php echo $newsimage; ?>);"></div>
                                            <?php

                                            the_title('<h3><a href="' . get_the_permalink() . '">', '</a></h3>');
                                            echo '<p>' . get_the_excerpt() . '</p>';
                                            ?>
                                            <div class="news-card--meta">
                                                <div class="row">
                                                    <div class="col-lg-6 col-xs-6">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                        <?php
                                                        //                                                echo get_the_date('m/d/Y');
                                                        echo get_the_date('F d, Y');
                                                        ?>
                                                    </div>
                                                    <div class="col-lg-4 col-lg-offset-2 col-xs-6">
                                                        <?php
                                                        echo ' <a href="' . get_the_permalink() . '" class="btn btn-primary btn-sm btn-block read-more">READ MORE</a>';
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;
                                ?>
                            </div>
                            <?php
                            echo '<div class="text-center lead">';
                            echo paginate_links(array(
                                'prev_text' => '<<',
                                'next_text' => '>>',
                            ));
                            echo '</div>';

                        endif;
                        ?>
                    </div>

                    <?php
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>