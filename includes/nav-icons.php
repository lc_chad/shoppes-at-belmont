<?php
	
	$icons = [];
	
	$icons['contact'] = array(
			'site_phone' => array('class' => 'fas fa-phone', 'url-prefix' => 'tel:'),
			'site_email' => array('class' => 'fas fa-envelope', 'url-prefix' => 'mailto:'),
	);
	
	$icons['social'] = array(
			'social_twitter' => array('class' => 'fab fa-twitter'),
			'social_facebook' => array('class' => 'fab fa-facebook-f'),
			'social_linkedin' => array('class' => 'fab fa-linkedin-in'),
			'social_youtube' => array('class' => 'fab fa-youtube'),
			'social_instagram' => array('class' => 'fab fa-instagram'),
			'social_pinterest' => array('class' => 'fab fa-pinterest-p'),
			'social_vimeo' => array('class' => 'fab fa-vimeo-v'),
			'social_snapchat' => array('class' => 'fab fa-snapchat-ghost'),
			'social_rss' => array('class' => 'fas fa-rss', 'url' => '/feed/'),
	);