<?php if ( get_field('lc-intro__enabled') ) :
	$intro = get_field('lc-intro');
	$content_class = '';
	?>
	<div class="intro-wrapper">
		<section id="intro" class="intro container-fluid">
				<?php
				if ( $intro['lc-intro__image-type'] ) :
					if ( $intro['lc-intro__image-type'] == 'img' ) :
						$img = $intro['lc-intro__image']['sizes']['large'];
						$content_class = 'intro__content--has-image';
						?>
						<div class="intro__graphic intro__graphic--image">
							<img src="<?php echo $img; ?>" class="img-fluid" />
						</div>
					<?php
					elseif ( $intro['lc-intro__image-type'] == 'icon' ) :
						$content_class = 'intro__content--has-image';
						?>
						<div class="intro__graphic intro__graphic--icon">
							<i class="<?php echo $intro['lc-intro__icon']->class; ?>"></i>
						</div>
					<?php
					endif;
				endif; ?>
				<div class="intro__content <?php echo $content_class; ?>">
					<h2 class="intro__headline"><?php echo $intro['lc-intro__headline']; ?></h2>
					<div class="intro__body">
						<?php echo $intro['lc-intro__content']; ?>
					</div>
				</div>
		</section>
	</div>
<?php endif; ?>