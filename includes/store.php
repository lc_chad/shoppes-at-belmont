<?php
// TAXONOMY
	add_action('init', 'lc_custom_register_store_types', 0);
	
	function lc_custom_register_store_types()
	{
		
		$labels = array(
				'name' => _x('Types', 'taxonomy general name'),
				'singular_name' => _x('Type', 'taxonomy singular name'),
				'search_items' => __('Search Types'),
				'all_items' => __('All Types'),
				'parent_item' => __('Parent Type'),
				'parent_item_colon' => __('Parent Type:'),
				'edit_item' => __('Edit Type'),
				'update_item' => __('Update Type'),
				'add_new_item' => __('Add New Type'),
				'new_item_name' => __('New Type Name'),
				'menu_name' => __('Types'),
		);
		
		register_taxonomy('store-types', array('post'), array(
				'hierarchical' => true,
				'labels' => $labels,
				'show_ui' => true,
				'show_admin_column' => true,
				'query_var' => true,
				'meta_box_cb' => false,
				'rewrite' => array('slug' => 'store-types'),
		));
	}

// CUSTOM POST TYPE
	add_action('init', 'lc_custom_register_store');
	function lc_custom_register_store()
	{
		$labels = array(
				'name' => 'Stores',
				'singular_name' => 'Store',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Store',
				'edit_item' => 'Edit Store',
				'new_item' => 'New Store',
				'view_item' => 'View Store',
				'search_items' => 'Search Stores',
				'not_found' => 'Nothing Found',
				'not_found_in_trash' => 'Nothing Found in Trash',
				'parent_item_colon' => ''
		);
		$args = array(
				'labels' => $labels,
				'public' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => true,
				'show_ui' => true,
				'query_var' => true,
				'menu_icon' => 'dashicons-store',
				'rewrite' => true,
				'capability_type' => 'post',
				'hierarchical' => true,
				'menu_position' => null,
				'taxonomies' => array('store-types'),
				'supports' => array('title', 'thumbnail'),
//        'supports' => array('title', 'page-attributes', 'thumbnail'),
		);
		register_post_type('store', $args);
	}
	
	/*ACF
	=====
	lc-store__logo
	lc-store__description
	lc-store__photo
	lc-store__hours
	lc-store__phone
	lc-store__address
	lc-store__building
	lc-store__suite
	lc-store__address
	lc-store__url
	lc-store__social
	lc-store--anchor
	*/

// AFFILIATE CLASS
	class LC_Store
	{
		public $args;
		public $query;
		
		function __construct($args)
		{
			$this->args = $args;
		}
		
		public function getPresentation($anchors_only = false)
		{
			$result = '';
			
			$this->query = new WP_Query($this->args);

//        var_dump( $this->query );
			
			while ($this->query->have_posts()) : $this->query->the_post();
					$class = '';
					$url = get_permalink();
					$img = get_field('lc-store__logo');
					$img = $img['sizes']['medium'];
					$name = get_the_title();
					
					$result .= '
                <div class="store ' . $class . '">
                    <a href="' . $url . '" class="store__link"></a>
                    <img src="' . $img . '" class="store__logo" alt="'.$name.'" title="'.$name.'" />
                </div>
				';
			endwhile;
			
			wp_reset_query();
			
			if (!empty($result)) {
				$result = '<div class="stores">' . $result . '</div>';
			}
			
			return $result;
		}
	}
	
	add_shortcode('stores', 'store_shortcode');
	function store_shortcode($atts)
	{
		$atts = shortcode_atts(array(
				'type' => false,
				'rand' => false,
				'limit' => -1,
		), $atts, 'stores');
		$result = '';
		
		$args = array(
				'posts_per_page' => $atts['limit'],
				'post_type' => 'store',
				'orderby' => 'menu_order title',
				'order' => 'ASC'
		);
		
		if ($atts['rand']) {
			$args['orderby'] = 'rand';
		}
		
		if ($atts['type'] !== FALSE) {
			$args['tax_query'] = array(
					array(
							'taxonomy' => 'store-types',
							'field' => 'slug',
							'terms' => $atts['type'],
					),
			);
		}
		
		$store = new LC_Store($args);
		
		if ($store !== false) {
			$result = $store->getPresentation();
		}
		
		return $result;
	}