<section id="connect" class="connect">
	<div class="container-fluid container--narrow">
		
		<header class="connect__header header header--hr">
			<a href="#connect__form" class="connect__cover-link" data-toggle="collapse" aria-expanded="false" aria-controls="connect__cover-link"></a>
			<h2 class="connect__headline header__headline">
				<span class="headline--co">Contact</span>
			</h2>
			<div class="connect__prompt connect__prompt--hr">
				<?php if ( !empty(get_theme_mod('contact_form_preamble'))) : echo get_theme_mod('contact_form_preamble'); else: ?>
					<span class="prompt__cta" data-collapsed="Click here">Complete the form below</span> to send us questions or requests.
				<?php endif; ?>
			</div>
		</header>
		
		<div class="connect__form <?php echo ( !$GLOBALS['expand_contact_form'] ) ? 'collapse' : ''; ?>" id="connect__form">
			<?php gravity_form(1, false, false); ?>
		</div>
		
	</div>
</section>