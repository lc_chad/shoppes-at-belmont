<?php
	// CUSTOM POST TYPE
	add_action('init', 'lc_register_cta');
	function lc_register_cta()
	{
		$labels = array(
			'name' => 'CTAs',
			'singular_name' => 'CTA',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New CTA',
			'edit_item' => 'Edit CTA',
			'new_item' => 'New CTA',
			'view_item' => 'View CTA',
			'search_items' => 'Search CTAs',
			'not_found' => 'Nothing Found',
			'not_found_in_trash' => 'Nothing Found in Trash',
			'parent_item_colon' => ''
		);
		$args = array(
			'labels' => $labels,
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_icon' => 'dashicons-admin-links',
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'menu_position' => null,
			'supports' => array('title', 'page-attributes'),
		);
		register_post_type('cta', $args);
	}
	
	/*ACF
	=====
	lc-cta__photo
	lc-cta__url
	*/

	// AFFILIATE CLASS
	class LC_CTA
	{
		public $args;
		public $query;
		
		function __construct($args)
		{
			$this->args = $args;
		}
		
		public function getPresentation()
		{
			$result = '';
				
			$this->query = new WP_Query($this->args);
			
			while ($this->query->have_posts()) : $this->query->the_post();
				$url = get_field('lc-cta__url');
				$img = get_field('lc-cta__photo');
				$img = $img['sizes']['medium-large'];
				$name = get_the_title();
				
				$result .= '
								<div class="cta">
										<a href="' . $url . '" class="cta__link" target="_blank"></a>
										<h2 class="cta__name">'.$name.'</h2>
										<div class="cta__background" style="background-image:url(\''.$img.'\')"></div>
								</div>
					';
			endwhile;
			
			wp_reset_query();
			
			$result = '<div class="ctas">'.$result.'</div>';
			
			return $result;
		}
	}
	
	add_shortcode('lc_ctas', 'lc_cta_shortcode');
	function lc_cta_shortcode($atts)
	{
		$atts = shortcode_atts(array(
			'limit' => 3,
		), $atts, 'lc_ctas');
		$result = '';
		
		$args = array(
			'posts_per_page' => $atts['limit'],
			'post_type' => 'cta',
			'orderby' => 'menu_order title',
			'order' => 'ASC'
		);
		
		$cta = new LC_CTA($args);
		
		if ($cta !== false) {
			$result = $cta->getPresentation();
		}
		
		return $result;
	}