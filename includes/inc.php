<?php
	
include 'store.php';
include 'cta.php';

function lc_get_contact_links($icons)
{
	if (empty($icons)) :
		$icons = array(
				'site_phone' => array('class' => 'fas fa-phone', 'url-prefix' => 'tel:'),
				'site_email' => array('class' => 'fas fa-envelope', 'url-prefix' => 'mailto:'),
		);
	endif;
	
	$html = false;
	foreach ($icons as $value => $settings) {
		if (!empty(get_theme_mod($value))):
			$url = !empty($settings['url-prefix']) ? $settings['url-prefix'] . get_theme_mod($value) : get_theme_mod($value);
			$html .= '<div class="contact-type contact-item icon menu-item item"><a href="' . $url . '"> <i class="' . $settings['class'] . '"></i></a></div>';
		endif;
	}
	
	return $html;
}

function lc_get_icon_links($icons = false) {
	if ( !is_array($icons) ) { return false; }
	$html = '';
	
	foreach ( $icons as $value=>$settings ) {
		if( !empty(get_theme_mod($value))):
			$url = !empty($settings['url']) ? $settings['url'] : esc_url(get_theme_mod($value));
			$html .= '<div class="nav-item nav-item--icon"><a href="'.$url.'" target="_blank" class="nav-item__link nav-item__link--icon"> <i class="'.$settings['class'].' nav-item__icon"></i></a></div>';
		endif;
	}
	
	return $html;
}

function mark_first_word( $str, $first_class = 'first', $second_class='second' ) {
	$pieces = explode(' ', $str, 2);
	$str = '<span class="'.$first_class.'">' . $pieces[0] . '</span> ' . '<span class="'.$second_class.'">'.$pieces[1].'</span>';
	
	return $str;
}