<?php
	$args = array(
			'posts_per_page' => 2,
			'orderby' => 'date',
			'order' => 'DESC',
	);
	$featured = new WP_Query($args);
	$counter = 0;
	
	if ($featured->have_posts()): ?>
		<section id="news" class="news">
			<div class="container-fluid">
				<h2 class="news__headline">What's Happening at Belmont</h2>
				<div class="news-items">
					<?php while ($featured->have_posts()): $featured->the_post();
						
						$thumbnail = false;
						$class = 'no-thumbnail';
						
						if (!empty(get_field('lc_post_thumbnail'))) {
							$thumbnail = get_field('lc_post_thumbnail');
						} else if (!empty(get_the_post_thumbnail_url())) {
							$thumbnail = get_the_post_thumbnail_url(null, 'large');
						}
						
						if ($thumbnail) {
							$class = ' has-thumbnail';
						}
						
						$date = get_the_date('m.j.Y');
						
						$class = '';
						
						?>
						<div class="news-item <?php echo $class; ?>">
							<figure class="news-item__image"><a href="<?php the_permalink(); ?>"
																									class="news-item__link"></a>
								<div class="news-item__cover image__cover" <?php
									if ($thumbnail): ?>style="background-image: url('<?php echo $thumbnail; ?>');"<?php endif;
								?>></div>
							</figure>
							<div class="news-item__content">
								<h5 class="news-item__title"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h5>
								<h6 class="news-item__date"><?php echo $date; ?></h6>
								<div class="news-item__excerpt"><?php the_excerpt(); ?></div>
								<p class="news-item__read-more"><a href="<?php the_permalink(); ?>" class="read-more__link">Read
										more</a>
								</p>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
			
			<div class="news__read-more text-center">
				<a href="/news" class="btn primary-btn">View More News</a>
			</div>
		</section>
	
	<?php endif; ?>