jQuery(document).ready(function($){
	
	
	if ( $('body.home').length > 0 ) {
		$(window).on('resize load lc-load', function () {
			let $content = $('.news-item__content');
			let h = 0;
			
			$content.each(function(){
				if ( $(this).outerHeight() > h ) {
					h = $(this).outerHeight();
				}
			});
			
			$content.css('min-height',h+'px');
		});
	}
	
	let $page = $('html, body');
	let scroll_events = 'scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove';
	
	$('[data-slide-to]').each(function(){
		$(this).on( 'click mousedown tapstart', function(){
			let target = $(this).data('slide-to');
			let pos = $('#'+target).offset().top;
			
			$page.on( scroll_events, function(){
				$page.stop();
			});
			
			$page.animate({
				scrollTop: pos
			}, 2000, function(){
				$page.off(scroll_events);
			});
		});
	});
	
	
	
	$('.main-nav--jump-links .nav__item a, .nav__branding a, .footer__logo a').each(function(){
		$(this).on( 'click mousedown tapstart', function(e){
			e.preventDefault();
			
			$('html, body').queue([]).stop();
			
			let target = $($(this).attr('href'));
			let pos = $(target).offset().top - $('#head').outerHeight() - 10;
			
			$('html, body').animate({
				scrollTop: pos
			}, 2000);
			
			if ($('.main-nav').hasClass('show')) {
				$('.main-nav').removeClass('show');
			}
		});
	});
	
	$(document).on( 'touchmove mousewheel', function(){
		$('html, body').queue([]).stop();
	});
	
	$(window).on( 'click', function(e) {
		if ($('#main-nav').hasClass('show')) {
			$('#main-nav').removeClass('show')
		}
	});

	let $slideshow = $('.hero--slideshow');
	let slideshow_timeouts = [];
	if ( $slideshow.length > 0 ) {
		let slideDelay = 5000;
		$slideshow.each(function(){
			let $ss = $(this);
			let ss_id = 'ss'+$ss.index();
			let $slides = $ss.find('.hero__image');

			if ( $slides.length > 1 ) {
				let $current = $slides.eq(0);
				$ss.data('currentSlide', $current);
				$ss.data('nextSlide', $current.next());
				$ss.data('slides', $slides);

				$ss.on('slide-transition-complete', function(e){
					let $ss = $(this);
					let $slides = $ss.data('slides');
					let $current = $ss.data('currentSlide');
					let $next = $current.next();
					if ( $next.length === 0 ) {
						$next = $slides.first();
					}
					$ss.data('nextSlide', $next);

					$ss.data('slides').css('z-index', 5);
					$current.css('z-index', 10);
					$next.hide().css('z-index',100); // prepping for fade in

					slideshow_timeouts[ss_id] = setTimeout( function(){nextSlide($ss)}, slideDelay );
				});

				// kick off...
				$ss.data('slides').css('z-index', 5);
				$current.css('z-index', 10);
				$ss.data('nextSlide').hide().css('z-index',100);
				slideshow_timeouts[ss_id] = setTimeout( function(){nextSlide($ss)}, slideDelay );
			}
		});
	}

	function nextSlide($ss) {
		let transitionSpeed = 500;
		let $slides = $ss.data('slides');
		let $current = $ss.data('currentSlide');
		let $next = $ss.data('nextSlide');

		$ss.data('currentSlide', $next);
		$ss.data('currentSlide').fadeIn(transitionSpeed, function(){$ss.trigger('slide-transition-complete');});
	}
});