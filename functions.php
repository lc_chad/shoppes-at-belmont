<?php
	
	add_action('wp_enqueue_scripts', 'lc_scripts');
	function lc_scripts()
	{
		//STYLES
		wp_enqueue_style('lc-stylesheet', get_stylesheet_uri());
		wp_enqueue_style('lc-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400|Volkhov:400,400i,700&display=swap');
		wp_enqueue_style('lc-slick-slider', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
		
		//SCRIPTS
		wp_enqueue_script('lc-bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array(), 1, true);
		wp_enqueue_script('jquery', false, array(), false, false);
//		wp_enqueue_script('lc-jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', array(), false, false);
		wp_enqueue_script('lc-slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), 1, true);
		wp_enqueue_script('lc-functions-js', get_template_directory_uri() . '/js/functions.js', array(), 1, true);
		
		if (!empty(get_theme_mod('tracking_maps_api_key'))) :
			echo '<!-- testing -->';
			wp_enqueue_script('lc-map-js', get_template_directory_uri() . '/js/map.js', array(), 1, false);
			wp_enqueue_script('lc-gmap-js',
					'https://maps.googleapis.com/maps/api/js?key=' . get_theme_mod('tracking_maps_api_key') . '&callback=GmapInit', array(), 1, true);
		endif;
		
		wp_localize_script('lc-functions-js', 'ajax_posts', array(
				'ajax_url' => admin_url('admin-ajax.php'),
				'check_nonce' => wp_create_nonce('lc-nonce'),
				'noposts' => __('No more posts to load.', 'lc')
		));
	}

//BOOTSTRAP NAVBAR NAVWALKER
	require_once('includes/class-wp-bootstrap-navwalker.php');

//<editor-fold desc="MENUS">
	add_action('after_setup_theme', 'lc_init');
	function lc_init()
	{
		register_nav_menus(array(
				'primary-menu' => __('Main Menu', 'lc'),
		));
		register_nav_menus(array(
				'top-menu' => __('Top Menu', 'lc'),
		));
		register_nav_menus(array(
				'footer-menu' => __('Footer Menu', 'lc'),
		));
	}

//</editor-fold>

//WIDGETS
	function lc_widgets_init()
	{
		$widgets_bag = array(
				array('name' => 'Footer Badge', 'id' => 'footer__badge'),
		);
		
		foreach ($widgets_bag as $item):
			register_sidebar(array(
					'name' => __($item['name'], 'lc'),
					'id' => $item['id'],
					'description' => '',
					'before_widget' => '<aside id="' . $item['id'] . '" class="widget ' . $item['id'] . '">',
					'after_widget' => '</aside>',
					'before_title' => '<h3 class="widget__title">',
					'after_title' => '</h3>'
			));
		endforeach;
	}
	
	add_theme_support('widgets');
	add_action('widgets_init', 'lc_widgets_init');

//THEME SUPPORT
	add_theme_support('post-thumbnails');
	add_theme_support('title-tag');
	add_theme_support('custom-logo');

//SUPPORT FOR WIDGETS TO COMPILE SHORTCODES
	add_filter('widget_text_content', 'do_shortcode', 50);

//CUSTOMIZE ADDITIONAL ITEMS
	add_action('customize_register', 'lc_customize_register');
	
	function lc_customize_register($wp_customize)
	{
		$wp_customize->add_setting('site_phone', array(
				'default' => '',
				'sanitize_callback' => 'esc_html',
		));
		$wp_customize->add_setting('site_email', array(
				'default' => '',
				'sanitize_callback' => 'esc_html',
		));
		$wp_customize->add_setting('site_address', array(
				'default' => '',
				'sanitize_callback' => 'esc_html',
		));
		$wp_customize->add_section('lc_social', array(
				'title' => __('Social', 'lc'),
		));
		$wp_customize->add_setting('social_twitter', array(
				'default' => '',
				'sanitize_callback' => 'esc_url',
		));
		$wp_customize->add_setting('social_facebook', array(
				'default' => '',
				'sanitize_callback' => 'esc_url',
		));
		$wp_customize->add_setting('social_instagram', array(
				'default' => '',
				'sanitize_callback' => 'esc_url',
		));
		$wp_customize->add_setting('social_linkedin', array(
				'default' => '',
				'sanitize_callback' => 'esc_url',
		));
		$wp_customize->add_setting('social_youtube', array(
				'default' => '',
				'sanitize_callback' => 'esc_url',
		));
		$wp_customize->add_setting('social_rss', array(
				'default' => '1',
		));
		
		$wp_customize->add_control('site_phone', array(
				'label' => 'Phone Number',
				'section' => 'title_tagline',
				'setting' => 'site_phone',
				'type' => 'text',
		));
		$wp_customize->add_control('site_email', array(
				'label' => 'Email',
				'section' => 'title_tagline',
				'setting' => 'site_email',
				'type' => 'email',
		));
		$wp_customize->add_control('site_address', array(
				'label' => 'Address',
				'section' => 'title_tagline',
				'setting' => 'site_address',
				'type' => 'email',
		));
		$wp_customize->add_control('social_twitter', array(
				'label' => 'Twitter URL',
				'section' => 'lc_social',
				'setting' => 'social_twitter',
				'type' => 'url'
		));
		$wp_customize->add_control('social_facebook', array(
				'label' => 'Facebook URL',
				'section' => 'lc_social',
				'setting' => 'social_facebook',
				'type' => 'url'
		));
		$wp_customize->add_control('social_instagram', array(
				'label' => 'Instagram URL',
				'section' => 'lc_social',
				'setting' => 'social_instagram',
				'type' => 'url'
		));
		$wp_customize->add_control('social_linkedin', array(
						'label' => 'LinkedIn URL',
						'section' => 'lc_social',
						'setting' => 'social_linkedin',
						'type' => 'url'
				)
		);
		$wp_customize->add_control('social_youtube', array(
						'label' => 'YouTube',
						'section' => 'lc_social',
						'setting' => 'social_youtube',
						'type' => 'url'
				)
		);
		$wp_customize->add_control('social_rss', array(
				'label' => 'Include RSS feed link to posts?',
				'section' => 'lc_social',
				'setting' => 'social_rss',
				'type' => 'checkbox'
		));
		
		// tracking codes
		$wp_customize->add_section('lc_tracking_codes', array(
				'title' => __('Tracking Codes', 'lc'),
				'description' => __('Paste tracking codes for various ad services.')
		));
		$wp_customize->add_setting('tracking_maps_api_key', array(
				'default' => '',
		));
		$wp_customize->add_control('tracking_maps_api_key', array(
				'label' => 'Google Maps API Key',
				'section' => 'lc_tracking_codes',
				'setting' => 'tracking_maps_api_key',
				'type' => 'text',
		));
		$wp_customize->add_setting('tracking_google', array(
				'default' => '',
		));
		$wp_customize->add_control('tracking_google', array(
				'label' => 'Google Analytics Tracking Code',
				'section' => 'lc_tracking_codes',
				'setting' => 'tracking_google',
				'type' => 'textarea',
		));
		$wp_customize->add_setting('tracking_fb', array(
				'default' => '',
		));
		$wp_customize->add_control('tracking_fb', array(
				'label' => 'Facebook Pixel Tracking Code',
				'section' => 'lc_tracking_codes',
				'setting' => 'tracking_fb',
				'type' => 'textarea',
		));
		$wp_customize->add_setting('tracking_callrail', array(
				'default' => '',
		));
		$wp_customize->add_control('tracking_callrail', array(
				'label' => 'CallRail Tracking Code',
				'section' => 'lc_tracking_codes',
				'setting' => 'tracking_callrail',
				'type' => 'textarea',
		));
		
		// tracking codes
		$wp_customize->add_section('lc_map', array(
				'title' => __('Map Settings', 'lc'),
				'description' => __('Set the lat/long for where the map is centered')
		));
		$wp_customize->add_setting('lc_map_lat', array(
				'default' => '',
		));
		$wp_customize->add_control('lc_map_lat', array(
				'label' => 'Latitude',
				'section' => 'lc_map',
				'setting' => 'lc_map_lat',
				'type' => 'text',
		));
		$wp_customize->add_setting('lc_map_lng', array(
				'default' => '',
		));
		$wp_customize->add_control('lc_map_lng', array(
				'label' => 'Longitude',
				'section' => 'lc_map',
				'setting' => 'lc_map_lng',
				'type' => 'text',
		));
		
		// custom footer logo
		$wp_customize->get_setting('custom_footer_logo')->transport = 'postMessage';
		
		$wp_customize->add_setting('custom_footer_logo', array(
				'default' => '',
				'type' => 'theme_mod',
				'capability' => 'edit_theme_options',
		));
		
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'custom_footer_logo', array(
						'label' => __('Footer Logo', 'shoppesab'),
						'section' => 'title_tagline',
						'settings' => 'custom_footer_logo',
						'priority' => 30
				))
		);
		
		$wp_customize->add_setting('default_footer', array(
				'default' => '',
				'type' => 'theme_mod',
				'capability' => 'edit_theme_options',
		));
		
		// fallback header
		$wp_customize->get_setting('default_hero_image')->transport = 'postMessage';
		$wp_customize->get_setting('default_footer_image')->transport = 'postMessage';
		
		$wp_customize->add_section('default_images', array(
				'title' => __('Default Imagery', 'lc'),
				'description' => __('Set the default header/hero and footer images when one isn\'t set for the page.')
		));
		
		$wp_customize->add_setting('default_hero', array(
				'default' => '',
				'type' => 'theme_mod',
				'capability' => 'edit_theme_options',
		));
		
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'default_hero', array(
						'label' => __('Hero/Header Background Image', 'shoppesab'),
						'section' => 'default_images',
						'settings' => 'default_hero',
				))
		);
		
		$wp_customize->add_setting('default_footer', array(
				'default' => '',
				'type' => 'theme_mod',
				'capability' => 'edit_theme_options',
		));
		
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'default_footer', array(
						'label' => __('Default Footer Background Image', 'shoppesab'),
						'section' => 'default_images',
						'settings' => 'default_footer',
				))
		);
	}

//CUSTOM FUNCTIONS
	require_once 'includes/inc.php';

//ACF GOOGLE MAP KEY
	function lc_acf_init()
	{
		if (!empty(get_theme_mod('tracking_maps_api_key'))) :
			acf_update_setting('google_api_key', get_theme_mod('tracking_maps_api_key'));
		endif;
	}
	
	add_action('acf/init', 'lc_acf_init');

//
// LIQUIFIED CREATIVE SUPPORT TICKET WIDGET
	function dashboard_widget_function($post, $callback_args)
	{
		echo '<a target="_blank" href="http://support.liquifiedcreative.com/">
					<img border="0" alt="Contact Liquified Creative for Support" src="https://www.liquifiedcreative.com/wp-content/uploads/2015/04/trouble-ticket.jpg" style="width: 350px;height: 278px">
					</a>';
	}
	
	function add_dashboard_widgets()
	{
		wp_add_dashboard_widget('dashboard_widget', 'Submit a Trouble Ticket', 'dashboard_widget_function');
	}
	
	add_action('wp_dashboard_setup', 'add_dashboard_widgets');
//END LC DASHBOARD WIDGET
//

//
//SHORTCODE SPACES FIX
	/**
	 * Fixes empty <p> and <br> tags showing before and after shortcodes in the
	 * output content.
	 */
	function pb_the_content_shortcode_fix($content)
	{
		$array = array(
				'<p>[' => '[',
				']</p>' => ']',
				']<br />' => ']',
				']<br>' => ']'
		);
		
		return strtr($content, $array);
	}
	
	add_filter('the_content', 'pb_the_content_shortcode_fix');
	add_filter('widget_text_content', 'pb_the_content_shortcode_fix');

//
// CUSTOMIZE THE LOGIN SCREEN
	function lc_login_logo()
	{ ?>
		<style type="text/css">
			body.login {
				background: #003A66;
			}
			
			body.login #login {
				background: #C5B783;
				padding: 8% 40px 40px;
			}
			
			body.login #login h1 a, body.login h1 a {
				background-image: url(<?php echo get_template_directory_uri() ?>/images/logo-white.png);
				padding-bottom: 30px;
				background-size: contain;
				width: auto;
			}
			
			body.login #login #backtoblog a, body.login #login #nav a, body.login #login .privacy-policy-page-link a {
				color: #fff;
			}
			
			body.login #login #backtoblog a:hover, body.login #login #nav a:hover {
				color: #bababa;
			}
		</style>
	<?php }
	
	add_action('login_enqueue_scripts', 'lc_login_logo');
	
	function lc_login_logo_url()
	{
		return home_url();
	}
	
	add_filter('login_headerurl', 'lc_login_logo_url');
	
	function lc_login_logo_url_title()
	{
		return get_bloginfo('title');
	}
	
	add_filter('login_headertitle', 'lc_login_logo_url_title');

// ADMIN STYLE
//	function load_custom_wp_admin_style()
//	{
//		wp_register_style('custom_wp_admin_css', get_template_directory_uri() . '/admin.css', false, '1.0.0');
//		wp_enqueue_style('custom_wp_admin_css');
//
//		wp_register_script('custom_wp_admin_js', get_template_directory_uri() . '/js/admin.js', false, '1.0.0');
//		wp_enqueue_script('custom_wp_admin_js');
//	}
//
//	add_action('admin_enqueue_scripts', 'load_custom_wp_admin_style');

// ALLOWED FILE TYPES
//add_filter( 'upload_mimes', 'lc_file_types', 1, 1 );
//function lc_file_types( $file_types ) {
//	$new_filetypes = array();
//	$new_filetypes['svg'] = 'image/svg+xml';
//	$file_types = array_merge($file_types, $new_filetypes );
//	return $file_types;
//}

// IMAGE SIZES
	add_image_size('hero', 1920, 1080);
	add_image_size('medium-large', 900, 900);
	
	add_filter('image_size_names_choose', 'wpshout_custom_sizes');
	function wpshout_custom_sizes($sizes)
	{
		return array_merge($sizes, array(
				'hero' => __('Hero/Header'),
				'medium-large' => __('Medium-Large'),
		));
	}
	
	function lc_get_image_id($image_url)
	{
		global $wpdb;
		$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url));
		return $attachment[0];
	}
	
	
	function lc_load_more_posts_scripts($query = null)
	{
		
		if (empty($query)) {
			global $wp_query;
			$query = $wp_query;
		}
		
		wp_register_script('lc_loadmore', get_stylesheet_directory_uri() . '/load-more.js', array('jquery'));
		
		wp_localize_script('lc_loadmore', 'lc_loadmore_params', array(
				'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
				'posts' => json_encode($query->query_vars),
				'current_page' => get_query_var('paged') ? get_query_var('paged') : 1,
				'max_page' => $query->max_num_pages
		));
		
		wp_enqueue_script('lc_loadmore');
	}
	
	add_action('wp_enqueue_scripts', 'lc_load_more_posts_scripts');
	
	function lc_loadmore_ajax_handler(){
		
		$args = json_decode( stripslashes( $_POST['query'] ), true );
		$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
		$args['post_status'] = 'publish';
		
		query_posts( $args );
		
		$template = empty($_POST['post_template']) ? '' : 'template-parts/post/store';
		$format = empty($_POST['post_format']) ? '' : 'grid';
		
		if( have_posts() ) :
			
			while( have_posts() ): the_post();
		
				get_template_part( $template, $format );
			
			endwhile;
		
		endif;
		die;
	}
	
	add_action('wp_ajax_loadmore', 'lc_loadmore_ajax_handler'); // wp_ajax_{action}
	add_action('wp_ajax_nopriv_loadmore', 'lc_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}