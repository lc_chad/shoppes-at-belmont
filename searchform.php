<form role="search" method="get" class="search-form" action="/">
    <div class="input-group">
        <input type="search" class="form-control"
               placeholder="<?php echo esc_attr_x('Search …', 'placeholder') ?>"
               value="<?php echo get_search_query() ?>" name="s"
               title="<?php echo esc_attr_x('Search for:', 'label') ?>"/>
        <span class="input-group-btn">
            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
        </span>
    </div>
</form>