<?php
	/**
	 * Template Name: Store Listing
	 */
	get_header();
	
	$stores_per_page = 6;
	
	$GLOBALS['lc_query_filter'] = array(
			'store-types' => 'type',
	);

	// BUILD TAX QUERY FOR FILTER OPTIONS
	$filter_tax_query = array();
	$filter_list = array();
	
	foreach ($GLOBALS['lc_query_filter'] as $key => $filter):
		$filter_list[$filter] = array();
		
		$filter_value = get_query_var($filter);
		
		$filter_tax_query[] = array(
				'taxonomy' => $key,
				'field' => 'slug',
				'terms' => $filter_value,
		);
		
		$filter_list[$filter] = $filter_value;
	endforeach;
	
	$args = array(
			'posts_per_page' => $stores_per_page,
			'post_type' => 'store',
			'orderby' => 'menu_order title',
			'order' => 'ASC'
	);

	// SET PAGE FORM PAGING
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args['paged'] = $paged;
	
	$listing_query = new WP_Query( $args );
	
	
?>
<?php get_header();
	
	/* CONTENT */
	if (have_posts()):
		$title = get_the_title(); // default, but let's pull the category (and go back?? maybe use session for this)
		$title = mark_first_word($title, 'first', 'alt');
		?>
		
		<header class="content__header" id="content-header">
			<div class="container-fluid">
				<h1 class="content__title"><?php echo $title; ?></h1>
			</div>
			<nav class="content__nav">
				<div class="container-fluid">
					<?php /* TODO: build nav */ ?>
				</div>
				<a href="/map" class="content__header__map-link btn button">Directory Map</a>
			</nav>
		</header>
		
		<section id="content" class="content">
			<div class="container-fluid">
				<?php
					while (have_posts()):
						the_post();
						
						the_content();
					endwhile;
					
					/* STORES */
					if ( $listing_query->have_posts() ): ?>
						<div class="stores">
							<?php while ( $listing_query->have_posts() ) : $listing_query->the_post();
							get_template_part( '/template-parts/post/store', 'grid' );
							endwhile; ?>
						</div>
					<?php endif;
					
					if ($listing_query->max_num_pages > 1) :
						?>
						<p class="text-center">
							<button id="load-more" class="btn btn-primary">LOAD MORE</button>
						</p>
					<?php
					endif;
					
					wp_reset_query();
				?>
			</div>
		</section>
	
	
	<?php
	endif;
	/* END CONTENT */
	
	get_footer();