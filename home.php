<?php get_header();
	
	$p = 0;
	if (isset($_GET)):
		$p = (isset($_GET['page']) ? (int)$_GET['page'] : 1);
	endif;
	
	$title = get_the_title(get_option('page_for_posts')); // default, but let's pull the category (and go back?? maybe use session for this)
	$title = mark_first_word($title, 'first', 'alt');
?>
	
	<header class="content__header" id="content-header">
		<div class="container-fluid">
			<h1 class="content__title"><?php echo $title; ?></h1>
		</div>
	</header>

<?php
	/* CONTENT */
	$per_page = 4;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array(
			'posts_per_page' => -1,
			'paged' => $paged
	);
	
	$the_query = new WP_Query($args);

?>
	<section id="content" class="content">
		<div class="container-fluid">
			<?php
				$args = array(
						'post_type' => 'post',
						'posts_per_page' => $per_page,
				);
				if (!empty($p)) {
					$args['paged'] = $p;
				}
				
				$query = new WP_Query($args);
				
				$count = $query->post_count;
				
				if ($query->have_posts()):
					?>
					<section class="news-items">
						<?php
							$counter = 0;
							while ($query->have_posts()): $query->the_post();
								$thumbnail = false;
								$class = 'no-thumbnail';
								
								if (!empty(get_field('lc_post_thumbnail'))) {
									$thumbnail = get_field('lc_post_thumbnail');
								} else if (!empty(get_the_post_thumbnail_url())) {
									$thumbnail = get_the_post_thumbnail_url(null, 'large');
								}
								
								if ($thumbnail) {
									$class = ' has-thumbnail';
								}
								
								$date = get_the_date('m.j.Y');
								
								$class = '';
								
								?>
								<div class="news-item <?php echo $class;?>">
									<figure class="news-item__image"><a href="<?php the_permalink(); ?>"
																											class="news-item__link"></a>
										<div class="news-item__cover image__cover" <?php
											if ($thumbnail): ?>style="background-image: url('<?php echo $thumbnail; ?>');"<?php endif;
										?>></div>
									</figure>
									<div class="news-item__content">
										<h5 class="news-item__title"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h5>
										<h6 class="news-item__date"><?php echo $date; ?></h6>
										<div class="news-item__excerpt"><?php the_excerpt(); ?></div>
										<p class="news-item__read-more"><a href="<?php the_permalink(); ?>" class="read-more__link">Read more</a>
										</p>
									</div>
								</div>
							
							<?php
							endwhile;
						?>
					</section>
					<?php
					
					if ($query->found_posts > $per_page) :
						/* PAGINATION HERE */
						?>
						<div class="pagination">
						<?php
						echo paginate_links(array(
								'total' => $query->max_num_pages,
								'current' => $p,
								'format' => '?page=%#%',
								'show_all' => false,
								'type' => 'plain',
								'end_size' => 2,
								'mid_size' => 1,
								'prev_next' => true,
								'prev_text' => __('« Previous'),
								'next_text' => __('Next »'),
								'add_fragment' => '',
						)); ?>
						</div>
					<?php
					endif;
				
				endif;
			?>
		</div>
	</section>
	<?php include 'includes/contact.php'; ?>
<?php get_footer();